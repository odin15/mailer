<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class StartCommand extends Command
{
    public static $defaultName = 'app:start';

    protected function configure()
    {
        $this->setDescription('Consume AMQP messages');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting AMQP Consumer');

        /** @var Process[] $processes */
        $processes = [
            'AMQP' => new Process(['php', 'bin/console', 'messenger:consume', '-vvv']),
        ];

        while (true) {
            foreach ($processes as $name => $process) {
                if (!$process->isRunning()) {
                    $process->start(function ($type, $buffer) use($name, $output) {
                        $output->write('['.$name.'] '.$buffer);
                    });
                }
            }
            sleep(10);
        }
    }
}
